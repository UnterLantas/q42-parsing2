package itis.parsing2;

import itis.parsing2.annotations.Concatenate;
import itis.parsing2.annotations.NotBlank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class FactoryParsingServiceImpl implements FactoryParsingService {

    @Override
    public Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException {
        File file = new File(factoryDataDirectoryPath);
        ArrayList<File> files = new ArrayList<>();
        for (File file1 : file.listFiles()) {
            if (file1.isFile()) {
                files.add(file1);
            }
        }
        try {
            ArrayList<String> arrayList = new ArrayList<>();
            String ph;
            for (File currentFile : files) {
                FileReader fileReader = new FileReader(currentFile);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                while ((ph = bufferedReader.readLine()) != null) {
                    arrayList.add(ph);
                }
                fileReader.close();
                bufferedReader.close();
            }


            Map<String, String> map = new HashMap<>();
            for (String s : arrayList) {
                if (!s.equals("---")) {
                    String[] ph1 = s.split(":");
                    String field = ph1[0].replaceAll("\"", "").trim();
                    String value = "";
                    if (ph1.length>1){
                        value = ph1[1].replaceAll("\"", "").trim();
                    }
                    map.put(field, value);
                }
            }
            Class<Factory> factoryClass = Factory.class;
            Constructor constructor = factoryClass.getDeclaredConstructor();
            constructor.setAccessible(true);
            Factory factory = (Factory) constructor.newInstance();
            Field[] fields = factoryClass.getDeclaredFields();
            List<FactoryParsingException.FactoryValidationError> validationErrors = new ArrayList<>();
            for (Field field : fields) {
                field.setAccessible(true);
                Concatenate concatenate = field.getAnnotation(Concatenate.class);
                String fullName = "";
                if (concatenate != null) {
                    for (int i = 0; i < concatenate.fieldNames().length; i++) {
                        String ph1 = concatenate.fieldNames()[i];
                        fullName += map.get(ph1);
                        fullName += concatenate.delimiter();
                    }
                }

                NotBlank notBlank = field.getAnnotation(NotBlank.class);
                if (notBlank != null && map.get(field.getName()).equals("")) {
                    validationErrors.add(new FactoryParsingException.FactoryValidationError(field.getName(), "Поле не должно быть пустым"));
                }
                if (!fullName.equals("")) {
                    field.set(factory, fullName);
                } else if (field.getType() == List.class) {
                    List<String> list = Collections.singletonList(map.get(field.getName()));
                    field.set(factory, list);
                } else {
                    field.set(factory, map.get(field.getName()));
                }

            }
            if (validationErrors.size() != 0) {
                throw new FactoryParsingException("Ошибка: ", validationErrors);
            }
            return factory;
        } catch (FactoryParsingException e) {
            System.err.println(e.getMessage());
            System.err.println(e.getValidationErrors());
        } catch (IOException | NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
}
